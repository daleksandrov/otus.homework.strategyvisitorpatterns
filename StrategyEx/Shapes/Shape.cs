﻿namespace StrategyEx
{
    public class Shape
    {
        public Shape(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;

        public virtual string Export(IVisitor visitor)
        {
            return visitor.Export(this);
        }
    }
}