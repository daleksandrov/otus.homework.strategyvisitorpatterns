﻿namespace StrategyEx
{
    public class Rectangle: Shape
    {
        public int width, heiht;
        
        
        public Rectangle(int x, int y, int width, int heiht) : base(x, y)
        {
            this.width = width;
            this.heiht = heiht;
        }

        public override string Export(IVisitor visitor)
        {
            return visitor.Export(this);
        }
    }
}