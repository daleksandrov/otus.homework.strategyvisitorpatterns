﻿namespace StrategyEx
{
    public class Circle: Shape
    {
        public int radius;

        public Circle(int x, int y, int radius) : base(x,y)
        {
            this.radius = radius;
        }

        public override string Export(IVisitor visitor)
        {
            return visitor.Export(this);
        }
    }
}