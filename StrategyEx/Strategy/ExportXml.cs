﻿namespace StrategyEx
{
    public class ExportXml:IExport
    {
        public string Export(Shape shape)
        {
            return $@"<SHAPE><x>{shape.x}</x><y>{shape.y}</y></SHAPE>";
        }
    }
}