﻿namespace StrategyEx
{
    public class ExportJson:IExport
    {
        public string Export(Shape shape)
        {
            return $@"{{""x"":{shape.x}, ""y"":{shape.y}}}";
        }
    }
}