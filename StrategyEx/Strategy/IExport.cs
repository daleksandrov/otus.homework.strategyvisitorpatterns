﻿namespace StrategyEx
{
    public interface IExport
    {
        string Export(Shape shape);
    }
}