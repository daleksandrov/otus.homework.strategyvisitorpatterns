﻿using System;

namespace StrategyEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Shape(20, 35);
            Shape circle = new Circle(30, 50, 10);
            Shape rectangle = new Rectangle(10,20,5,7);
            
            IVisitor visitorJson = new VisitorJson();
            IVisitor visitorXml = new VisitorXML();
            
            Console.WriteLine(circle.Export(visitorJson));
            Console.WriteLine(rectangle.Export(visitorJson));
            Console.WriteLine(shape.Export(visitorJson));
            
            Console.WriteLine(circle.Export(visitorXml));
            Console.WriteLine(rectangle.Export(visitorXml));
            Console.WriteLine(shape.Export(visitorXml));
        }
    }
}