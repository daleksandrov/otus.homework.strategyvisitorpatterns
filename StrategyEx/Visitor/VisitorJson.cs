﻿namespace StrategyEx
{
    public class VisitorJson : IVisitor
    {
        public string Export(Shape shape)
        {
            return $@"{{""x"":{shape.x}, 
                        ""y"":{shape.y}}}";
        }

        public string Export(Circle cir)
        {
            return $@"{{""r"":{cir.radius}, 
                        ""x"":{cir.x}, 
                        ""y"":{cir.y}}}";
        }

        public string Export(Rectangle rectangle)
        {
            return $@"{{""w"":{rectangle.width},
                        ""h"":{rectangle.heiht},
                        ""x"":{rectangle.x},
                        ""y"":{rectangle.y}}}";
        }
    }
}