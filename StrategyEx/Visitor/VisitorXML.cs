﻿namespace StrategyEx
{
    public class VisitorXML: IVisitor
    {
        public string Export(Shape shape)
        {
            return $@"<SHAPE><x>{shape.x}</x><y>{shape.y}</y></SHAPE>";
        }

        public string Export(Circle circle)
        {
            return $@"<CIRCLE><x>{circle.x}</x><y>{circle.y}</y><R>{circle.radius}</R></CIRCLE>";
        }

        public string Export(Rectangle rectangle)
        {
            return $@"<Rectangle><x>{rectangle.x}</x><y>{rectangle.y}</y><W>{rectangle.width}</W><H>{rectangle.heiht}</H></Rectangle>";
        }
    }
}