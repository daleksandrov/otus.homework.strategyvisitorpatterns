﻿namespace StrategyEx
{
    public interface IVisitor
    {
        string Export(Shape shape);
        string Export(Circle circle);
        string Export(Rectangle rectangle);
    }
}